package njnu.opengms.container.vo;

import lombok.Data;

import java.util.Date;

/**
 * @ClassName RefactorMethodVO
 * @Description todo
 * @Author sun_liber
 * @Date 2018/12/19
 * @Version 1.0.0
 */
@Data
public class RefactorMethodVO {
    String id;
    String name;
    String description;
    Date createDate;
}
