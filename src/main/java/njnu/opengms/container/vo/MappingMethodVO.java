package njnu.opengms.container.vo;

import lombok.Data;

import java.util.Date;

/**
 * @ClassName MappingMethodVO
 * @Description todo
 * @Author sun_liber
 * @Date 2018/12/19
 * @Version 1.0.0
 */
@Data
public class MappingMethodVO {
    String id;
    String name;
    String description;
    Date createDate;
}
