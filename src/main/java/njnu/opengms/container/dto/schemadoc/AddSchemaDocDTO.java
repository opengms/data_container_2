package njnu.opengms.container.dto.schemadoc;

import com.ngis.udx.schema.UdxSchema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName AddSchemaDocDTO
 * @Description todo
 * @Author sun_liber
 * @Date 2018/12/17
 * @Version 1.0.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddSchemaDocDTO {
    String name;
    String detailMarkDown;
    String description;
    UdxSchema udxSchema;
}
