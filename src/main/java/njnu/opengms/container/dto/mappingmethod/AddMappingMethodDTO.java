package njnu.opengms.container.dto.mappingmethod;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName AddMappingMethodDTO
 * @Description todo
 * @Author sun_liber
 * @Date 2018/12/17
 * @Version 1.0.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddMappingMethodDTO {
    String name;
    String description;
    String detailMarkdown;
    String supportedUdxSchema;
    String storePath;
}
