package njnu.opengms.container.dto.refactormethod;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @ClassName AddRefactorMethodDTO
 * @Description todo
 * @Author sun_liber
 * @Date 2018/12/17
 * @Version 1.0.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddRefactorMethodDTO {
    String name;
    String description;
    String detailMarkdown;
    List<String> supportedUdxSchemas;
    String storePath;
}
