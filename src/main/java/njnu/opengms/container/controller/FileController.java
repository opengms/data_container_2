package njnu.opengms.container.controller;

import njnu.opengms.container.bean.JsonResult;
import njnu.opengms.container.enums.ResultEnum;
import njnu.opengms.container.exception.MyException;
import njnu.opengms.container.utils.ResultUtils;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * @ClassName FileController
 * @Description todo
 * @Author sun_liber
 * @Date 2018/12/18
 * @Version 1.0.0
 */
@RestController
@RequestMapping (value = "/file")
public class FileController {

    @Value ("${web.upload-path}")
    String upload;
    /**
     * 上传数据映射方法
     * @param file
     * @return
     */
    @RequestMapping (value = "/upload/{type}", method = RequestMethod.POST)
    JsonResult upload(@RequestParam ("file")MultipartFile file,
                      @PathVariable("type") String type) throws IOException {
        //TODO 这里仅仅完成了上传的功能（由用户下载到本地进行调用），并没有将数据映射方法解压缩，因此无法进行在线调用，同时也没有设计调用的运行记录的代码
        //TODO 因此在之后的版本中，需要加入在线调用的功能。
        String path;
        if(("map").equals(type)){
            path="services"+File.separator+"map"+File.separator;
        }else if(("refactor").equals(type)){
            path="services"+File.separator+"refactor"+File.separator;
        }else{
            throw new MyException(ResultEnum.UPLOAD_TYPRE_ERROR);
        }
        path+=(UUID.randomUUID().toString())+File.separator+file.getOriginalFilename();
        FileUtils.copyInputStreamToFile(file.getInputStream(),new File(upload+File.separator+path));
        return ResultUtils.success(path);
    }


    /**
     * 其实我们可以直接通过URL获取静态文件内容，不必使用该接口
     *
     * @param path
     * @return
     * @throws IOException
     */
    @RequestMapping (value = "/download", method = RequestMethod.GET)
    ResponseEntity<InputStreamResource> download(@RequestParam("path")String path) throws IOException {
        File file=new File(upload+File.separator+path);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Content-Disposition", "attachment;filename=" + file.getName());
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        return ResponseEntity
                .ok()
                .headers(headers)
                .contentLength(file.length())
                .body(new InputStreamResource(FileUtils.openInputStream(file)));
    }
}
