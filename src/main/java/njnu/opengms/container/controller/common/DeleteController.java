package njnu.opengms.container.controller.common;


import io.swagger.annotations.ApiOperation;
import njnu.opengms.container.annotation.SysLogs;
import njnu.opengms.container.bean.JsonResult;
import njnu.opengms.container.service.common.DeleteService;
import njnu.opengms.container.utils.ResultUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @InterfaceName DeleteController
 * @Description todo
 * @Author sun_liber
 * @Date 2018/9/8
 * @Version 1.0.0
 */
public interface DeleteController<UID,S extends DeleteService<UID>> {
    S getService();

    @RequestMapping (value = "/{id}", method = RequestMethod.DELETE)
    @ApiOperation (value = "删除")
    @SysLogs ("删除指定ID的Entity日志")
    default JsonResult delete(@PathVariable ("id") UID id){
        getService().remove(id);
        return ResultUtils.success(id+":删除成功");
    }
}
