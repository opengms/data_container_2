package njnu.opengms.container.controller.common;


import io.swagger.annotations.ApiOperation;
import njnu.opengms.container.annotation.SysLogs;
import njnu.opengms.container.bean.JsonResult;
import njnu.opengms.container.service.common.UpdateService;
import njnu.opengms.container.utils.ResultUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @InterfaceName UpdateController
 * @Description todo
 * @Author sun_liber
 * @Date 2018/9/8
 * @Version 1.0.0
 */
public interface UpdateController<UID,UD,S extends UpdateService<UID,UD>> {
    S getService();

    @RequestMapping (value = "/{id}", method = RequestMethod.PUT)
    @ApiOperation (value = "更新")
    @SysLogs ("更新指定ID的Entity日志")
    default JsonResult update(@PathVariable ("id") UID id, @RequestBody UD updateDTO){
        getService().update(id,updateDTO);
        return ResultUtils.success("更新成功");
    }
}
