package njnu.opengms.container.controller.common;


import njnu.opengms.container.service.common.BaseService;

/**
 * @InterfaceName BaseController
 * @Description todo
 * @Author sun_liber
 * @Date 2018/9/8
 * @Version 1.0.0
 */
public interface BaseController<E,AD,UD,FD,VO,UID,S extends BaseService<E,AD,UD,FD,VO,UID>> extends
        CreateController<AD,S>,
        DeleteController<UID,S>,
        QueryController<E,FD,VO,UID,S>,
        UpdateController<UID,UD,S>
{
}
