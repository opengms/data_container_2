package njnu.opengms.container.controller;

import com.ngis.udx.Transfer;
import com.ngis.udx.data.UdxData;
import com.ngis.udx.data.UdxNode;
import com.ngis.udx.schema.UdxSchema;
import njnu.opengms.container.bean.JsonResult;
import njnu.opengms.container.component.AsyncTaskComponent;
import njnu.opengms.container.component.People;
import njnu.opengms.container.dto.schemadoc.AddSchemaDocDTO;
import njnu.opengms.container.pojo.User;
import njnu.opengms.container.repository.SchemaDocRepository;
import njnu.opengms.container.service.MappingMethodServiceImp;
import njnu.opengms.container.service.SchemaDocServiceImp;
import njnu.opengms.container.utils.ResultUtils;
import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * @ClassName TestController
 * @Description todo
 * @Author sun_liber
 * @Date 2018/12/11
 * @Version 1.0.0
 */
@RestController
@RequestMapping (value = "/test")
public class TestController {

    @Autowired
    People people;

    @Autowired
    SchemaDocServiceImp schemaDocServiceImp;

    @Autowired
    SchemaDocRepository schemaDocRepository;

    @Autowired
    MappingMethodServiceImp mappingMethodServiceImp;


    @Autowired
    AsyncTaskComponent asyncTaskComponent;

    @RequestMapping (value="/async",method=RequestMethod.GET)
    public JsonResult async(){
        int count=10;
        for (int i = 0; i <count ; i++) {
            asyncTaskComponent.executeAsyncTask(i);
            asyncTaskComponent.executeAsyncTaskPlus(i);
        }
        return ResultUtils.success("异步支持");
    }


    @RequestMapping (value="/test1",method=RequestMethod.POST)
    public JsonResult test1(@RequestBody UdxData udxData) throws IOException {
        return  ResultUtils.success(udxData);
    }

    @RequestMapping (value="/test2",method=RequestMethod.POST)
    public JsonResult test2(@RequestBody UdxSchema udxSchema) throws IOException {
        return  ResultUtils.success(udxSchema);
    }


    @RequestMapping (value="/test3",method=RequestMethod.GET)
    public JsonResult test3(@RequestParam("id") String id){
        UdxSchema udxSchema=schemaDocServiceImp.get(id).getUdxSchema();
        UdxData udxData=Transfer.generate(udxSchema);
        return  ResultUtils.success(udxData);
    }

    @RequestMapping (value="/test4",method=RequestMethod.GET)
    public JsonResult test4(@RequestParam("id") String id){
        return  ResultUtils.success(mappingMethodServiceImp.findBySchema(id));
    }

    @RequestMapping (value="/test5",method=RequestMethod.GET)
    public JsonResult test5(@RequestParam("nodeName")String nodeName) throws IOException, DocumentException {
        UdxData udxData=Transfer.loadDataFromXmlFile(new File("C:\\Users\\sun_liber\\Desktop\\d\\data\\res_temperature_tiff.xml"));
        UdxNode udxNode=udxData.getNodeByName(nodeName);
        return  ResultUtils.success(udxNode);
    }

    @RequestMapping (value="/test6",method=RequestMethod.GET)
    public JsonResult test6() throws IOException, DocumentException {
        UdxSchema udxSchema=Transfer.loadSchemaFromXmlFile(new File("C:\\Users\\sun_liber\\Desktop\\d\\schema\\plot.xml"));
        AddSchemaDocDTO addSchemaDocDTO=new AddSchemaDocDTO();
        addSchemaDocDTO.setName("Plot Data for Forest");
        addSchemaDocDTO.setDescription("Plot Data for Forest");
        addSchemaDocDTO.setDetailMarkDown("Plot Data for Forest\n");
        addSchemaDocDTO.setUdxSchema(udxSchema);
//        schemaDocServiceImp.add(addSchemaDocDTO);
        return ResultUtils.success("1");
    }


    @RequestMapping (value="/test7",method=RequestMethod.POST)
    public JsonResult test7(@RequestParam ("file")MultipartFile file) throws IOException, DocumentException {
        File fileNormal=File.createTempFile(UUID.randomUUID().toString(),file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".")));
        file.transferTo(fileNormal);
        UdxData udxData=Transfer.loadDataFromXmlFile(fileNormal);
        return ResultUtils.success(udxData);
    }


    @RequestMapping (value="/test8",method=RequestMethod.POST)
    public JsonResult test8(@RequestBody User user) throws IOException, DocumentException {

        return ResultUtils.success(user);
    }

}
