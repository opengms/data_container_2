package njnu.opengms.container.controller;

import com.ngis.udx.Transfer;
import com.ngis.udx.schema.UdxSchema;
import njnu.opengms.container.bean.JsonResult;
import njnu.opengms.container.controller.common.BaseController;
import njnu.opengms.container.dto.mappingmethod.AddMappingMethodDTO;
import njnu.opengms.container.dto.mappingmethod.FindMappingMethodDTO;
import njnu.opengms.container.dto.mappingmethod.UpdateMappingMethodDTO;
import njnu.opengms.container.pojo.MappingMethod;
import njnu.opengms.container.service.MappingMethodServiceImp;
import njnu.opengms.container.utils.ResultUtils;
import njnu.opengms.container.vo.MappingMethodVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName MappingMethodController
 * @Description todo
 * @Author sun_liber
 * @Date 2018/12/17
 * @Version 1.0.0
 */
@RestController
@RequestMapping (value = "/map")
public class MappingMethodController implements BaseController<MappingMethod,AddMappingMethodDTO,UpdateMappingMethodDTO,FindMappingMethodDTO,MappingMethodVO,String,MappingMethodServiceImp> {
    @Autowired
    MappingMethodServiceImp mappingMethodServiceImp;

    @Override
    public MappingMethodServiceImp getService() {
        return this.mappingMethodServiceImp;
    }

    @RequestMapping (value="/generate",method=RequestMethod.POST)
    public JsonResult test3(@RequestBody UdxSchema udxSchema){
        return  ResultUtils.success(Transfer.generate(udxSchema));
    }


    //TODO 需要完成在线调用

}
