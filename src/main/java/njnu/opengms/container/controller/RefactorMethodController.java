package njnu.opengms.container.controller;

import njnu.opengms.container.controller.common.BaseController;
import njnu.opengms.container.dto.refactormethod.AddRefactorMethodDTO;
import njnu.opengms.container.dto.refactormethod.FindRefactorMethodDTO;
import njnu.opengms.container.dto.refactormethod.UpdateRefactorMethodDTO;
import njnu.opengms.container.pojo.RefactorMethod;
import njnu.opengms.container.service.RefactorMethodServiceImp;
import njnu.opengms.container.vo.RefactorMethodVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName RefactorMethodController
 * @Description todo
 * @Author sun_liber
 * @Date 2018/12/17
 * @Version 1.0.0
 */
@RestController
@RequestMapping (value = "/refactor")
public class RefactorMethodController implements BaseController<RefactorMethod,AddRefactorMethodDTO,UpdateRefactorMethodDTO ,FindRefactorMethodDTO,RefactorMethodVO, String,RefactorMethodServiceImp> {
    @Autowired
    RefactorMethodServiceImp refactorMethodServiceImp;

    @Override
    public RefactorMethodServiceImp getService() {
        return this.refactorMethodServiceImp;
    }

    //TODO 需要完成在线调用
}
