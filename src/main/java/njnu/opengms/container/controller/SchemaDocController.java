package njnu.opengms.container.controller;

import njnu.opengms.container.controller.common.BaseController;
import njnu.opengms.container.dto.schemadoc.AddSchemaDocDTO;
import njnu.opengms.container.dto.schemadoc.FindSchemaDocDTO;
import njnu.opengms.container.dto.schemadoc.UpdateSchemaDocDTO;
import njnu.opengms.container.pojo.SchemaDoc;
import njnu.opengms.container.service.SchemaDocServiceImp;
import njnu.opengms.container.vo.SchemaDocVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName SchemaDocController
 * @Description todo
 * @Author sun_liber
 * @Date 2018/12/17
 * @Version 1.0.0
 */
@RestController
@RequestMapping (value = "/schemaDoc")
public class SchemaDocController implements BaseController<SchemaDoc,AddSchemaDocDTO,UpdateSchemaDocDTO,FindSchemaDocDTO,SchemaDocVO,String,SchemaDocServiceImp> {

    @Autowired
    SchemaDocServiceImp schemaDocServiceImp;

    @Override
    public SchemaDocServiceImp getService() {
        return this.schemaDocServiceImp;
    }
}
