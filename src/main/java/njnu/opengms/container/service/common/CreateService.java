package njnu.opengms.container.service.common;

/**
 * @InterfaceName CreateService
 * @Description todo
 * @Author sun_liber
 * @Date 2018/9/8
 * @Version 1.0.0
 */
public interface CreateService<AD> {
    /**
     * 创建实体
     * @param addDTO 实体数据
     */
    void add(AD addDTO);
}
